import React from 'react'
import { Box, Button } from '../UI'
import { extratoLista } from '../../info'
import Items from '../Items'

const Extrato = () => {
    return (
        <Box>
            
            {extratoLista.updates.map((item) => <Items key={item.id} {...item} />)}
            
            <Button>
                Ver mais
            </Button>

        </Box>
    )
}

export default Extrato;