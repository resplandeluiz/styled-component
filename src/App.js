import React, { useState } from "react";
import { ThemeProvider } from 'styled-components'
import { temaClaro, temaEscuro } from './Components/UI/temas'
import { GlobalStyle } from './Components/GlobalStyle'

import Cabecalho from "./Components/Cabecalho";
import Container from "./Components/Container";
import { BTNTema } from "./Components/UI";
import SwitchTema from "./Components/SwitchTema";



function App() {

  const [tema, setTema] = useState(true)

  const toggleTema = () => setTema((v) => !v)

  return (
    <ThemeProvider theme={tema ? temaClaro : temaEscuro}>
      <GlobalStyle />
      <BTNTema onClick={toggleTema}>
        <SwitchTema tema={tema} />
      </BTNTema>
      <Cabecalho />
      <Container />
    </ThemeProvider>
  );
}

export default App;
